# chatkontrolle-subtitles

Here we provide subtitles for Alexander Lehmanns awesome video about
chatcontrol as planned by the European Commission.

  https://digitalcourage.video/w/a87cb889-65bd-440e-acc7-bade17d144cc

We welcome your contributions and fixes.

Especially translations to *other* languages are very much appreciated. Currently
subtitles are prepared/available for the following languages:

* German (avail.)
* English (avail.)
* French (prepared)
* Spanish (prepared)

We mainly used [Aegisub](http://www.aegisub.org/) for editing the `.srt` files. 

During preparation we also had lots of fun with speech-recognition done by
[vosk](https://alphacephei.com/vosk/). To create a first `.srt` file from the
original German video:

    $ pip3 install vosk
    $ vosk-transcriber -l de -i chatkontrolle.mp4 -t srt -i chatkontrolle-de.srt

You can contact us under `braunschweig@digitalcourage.de` if you need the
`.mp4` video sources or the `.wav` audio tracks. Please note that the video
linked to above also provides a download link which should be sufficient for
creating new subtitle files.

All stuff in here is provided under CC0.
