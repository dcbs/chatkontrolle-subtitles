1
00:00:00,433 --> 00:00:02,666
In the European Union there is a law about to be passed,

2
00:00:02,666 --> 00:00:07,983
that takes a dangerous turn in the trade-off
 between safety and freedom.

3
00:00:08,250 --> 00:00:12,566
The defined goal is positive:
Children should be better protected against abuse.

4
00:00:12,966 --> 00:00:18,650
But the law would have the side effect of creating an
extremely far-reaching surveillance infrastructure

5
00:00:18,650 --> 00:00:22,550
that has the potential to monitor us all,
everywhere, all the time.

6
00:00:22,550 --> 00:00:25,550
Because it would consist of our own devices.

7
00:00:26,183 --> 00:00:28,066
It is supposed to work like this:

8
00:00:28,366 --> 00:00:36,033
Providers of apps and platforms for communication would be
required to include an official monitoring function.

9
00:00:36,250 --> 00:00:44,066
 Images would be checked against a secret database,
chat messages or comments on social media would
be scanned for suspicious patterns.

10
00:00:44,066 --> 00:00:49,550
And for this to work, of course, the encryption of these
messages would then have to be bypassed.

11
00:00:49,550 --> 00:00:56,833
This is to be monitored by a central office, which will also
decide how and according to what content and criteria
searches are to be carried out.

12
00:00:56,833 --> 00:01:01,333
If something suspicious is found, the provider must report
the discovery to the investigating authorities.

13
00:01:01,833 --> 00:01:05,500
Of course, we could now go into great detail about

14
00:01:05,500 --> 00:01:13,933
what a disproportionate idea it is
to monitor the devices of everyone in the EU
without any suspicion by a central institution....

15
00:01:13,933 --> 00:01:18,599
About what consequences there would be
if this system is tampered with or hacked...

16
00:01:18,599 --> 00:01:21,150
Or what it would mean for a business location

17
00:01:21,150 --> 00:01:26,949
if all trade secrets could be scanned or leaked
from a nebulous black box...

18
00:01:27,683 --> 00:01:30,483
That this system will be expanded in the future

19
00:01:30,483 --> 00:01:37,500
to be used for prosecuting drug offenses, suspected
terrorism, or disturbing the public peace...

20
00:01:37,866 --> 00:01:44,983
And what it means for our democracy when the Chilling Effect
casts a large shadow over our society:

21
00:01:44,983 --> 00:01:52,199
Through leveraged encryption, the loss of trusted
communication, and the absence of privacy.

22
00:01:53,116 --> 00:01:55,716
But it is about protecting our children.

23
00:01:55,716 --> 00:02:00,699
And the question is whether there aren't other ways to
better protect children from abuse.

24
00:02:00,699 --> 00:02:05,000
So let's hear from those who have expertise
and experience in the matter:

25
00:02:05,250 --> 00:02:12,966
Child protection associations point out that encrypted
communication in the most widespread apps
hardly plays arole in this field,

26
00:02:12,966 --> 00:02:16,599
because the perpetrators usually use other channels to
exchange material.

27
00:02:17,199 --> 00:02:23,166
In Germany, we also have the case that about half of the
proceedings are directed against
children and adolescents themselves,

28
00:02:23,166 --> 00:02:28,016
 even though they have consensually and legally exchanged
their own intimate images.

29
00:02:28,016 --> 00:02:32,099
A problem that could be rapidly exacerbated by chat control,

30
00:02:32,099 --> 00:02:37,416
as the planned system focuses on channels that are
frequently used by children and young people in particular.

31
00:02:37,833 --> 00:02:39,710
Child protection associations demand:

32
00:02:39,710 --> 00:02:44,683
Better equipment and more specialists for the offices
responsible for protecting children.

33
00:02:44,900 --> 00:02:48,300
There is also a lack of mandatory training
in child protection:

34
00:02:48,300 --> 00:02:52,866
children need to know where to get help; what adults are
allowed to do and what not.

35
00:02:53,366 --> 00:02:59,800
And in order to be able to recognize possible victims
more quickly, teaching and educational staff
must be trained and sensitized.

36
00:03:00,516 --> 00:03:08,099
With this in mind, we are calling for the money
that is planned to be funneled into a
dystopian surveillance machine

37
00:03:08,099 --> 00:03:12,250
to be invested in real and effective
child protection instead.

38
00:03:12,449 --> 00:03:16,349
This gives us as a society the opportunity
to achieve two goals:

39
00:03:16,599 --> 00:03:20,566
Children and teenagers are protected more
effectively and sustainably from abuse.

40
00:03:20,566 --> 00:03:26,433
Confidential communication and privacy guaranteed by the
Charter of Fundamental Rights are preserved.

41
00:03:26,983 --> 00:03:30,983
Help us stop the law by spreading this video

42
00:03:30,983 --> 00:03:38,283
and go to "chat-kontrolle.eu"
to stay informed and learn what you can do.

