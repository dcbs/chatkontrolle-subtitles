﻿1
00:00:00,480 --> 00:00:02,916
In der EU soll ein neues Gesetz
verabschiedet werden,

2
00:00:02,910 --> 00:00:05,753
welches in der Abwägung
zwischen Freiheit und Sicherheit

3
00:00:05,750 --> 00:00:08,200
eine extrem gefährliche Richtung einschlägt.

4
00:00:08,200 --> 00:00:09,960
Das erklärte Ziel ist gut:

5
00:00:09,960 --> 00:00:12,846
Kinder sollen besser vor
Missbrauch geschützt werden.

6
00:00:12,846 --> 00:00:15,780
Doch das Gesetz hätte als Nebeneffekt eine extrem

7
00:00:15,780 --> 00:00:18,653
weitreichende Überwachungs-Infrastruktur zur Folge,

8
00:00:18,650 --> 00:00:19,820
die das Potenzial hat,

9
00:00:19,820 --> 00:00:22,593
uns alle immer und überall zu überwachen.

10
00:00:22,590 --> 00:00:25,920
Denn sie würde aus
unseren eigenen Geräten bestehen.

11
00:00:25,920 --> 00:00:28,373
Funktionieren soll das Ganze so:

12
00:00:28,373 --> 00:00:31,460
Anbieter von Apps und
Plattformen zur Kommunikation

13
00:00:31,460 --> 00:00:33,386
sollen gesetzlich dazu verpflichtet werden, 

14
00:00:33,380 --> 00:00:36,146
eine offizielle Überwachungs-Funktion einzubauen.

15
00:00:36,140 --> 00:00:39,020
Bilder würden mit einer
geheimen Datenbank abgeglichen,

16
00:00:39,020 --> 00:00:41,620
Chat-Nachrichten oder Kommentare
auf Social Media

17
00:00:41,620 --> 00:00:43,893
nach verdächtigen Mustern gescannt werden.

18
00:00:43,890 --> 00:00:45,820
Und damit das funktionieren kann, muss dann

19
00:00:45,820 --> 00:00:49,420
natürlich auch die Verschlüsselung
dieser Nachrichten umgangen werden.

20
00:00:49,420 --> 00:00:50,880
Kontrolliert werden soll das

21
00:00:50,880 --> 00:00:53,520
von *einer* zentralen Stelle, die auch entscheidet,

22
00:00:53,520 --> 00:00:55,980
wie und nach welchen Inhalten und Kriterien

23
00:00:55,980 --> 00:00:58,440
gesucht wird. Wird etwas Verdächtiges gefunden,

24
00:00:58,440 --> 00:01:01,666
muss der Anbieter den Fund den
Ermittlungsbehörden melden.

25
00:01:01,660 --> 00:01:04,486
Klar könnten wir jetzt ganz
detaillliert darauf eingehen,

26
00:01:04,480 --> 00:01:07,126
was für eine unverhältnismäßige Idee es ist,

27
00:01:07,120 --> 00:01:09,846
die Geräte von allen Menschen in der EU

28
00:01:09,846 --> 00:01:11,310
ohne jeglichen Verdacht

29
00:01:11,370 --> 00:01:14,026
durch eine zentrale Stelle zu überwachen... 

30
00:01:14,020 --> 00:01:16,613
Was für Konsequenzen es hat,
falls dieses System

31
00:01:16,610 --> 00:01:18,593
manipuliert oder gehackt wird...

32
00:01:18,590 --> 00:01:20,873
Oder was es für einen Wirtschaftsstandort bedeutet,

33
00:01:20,873 --> 00:01:24,420
wenn alle Geschäftsgeheimnisse von einer nebulösen

34
00:01:24,420 --> 00:01:27,593
Blackbox gescannt oder ausgeleitet werden könnten.

35
00:01:27,740 --> 00:01:30,480
Dass dieses System in Zukunft ausgeweitet wird,

36
00:01:30,570 --> 00:01:32,460
um es auch für die Verfolgung von

37
00:01:32,460 --> 00:01:35,790
Drogendelikten, Terror-Verdacht oder die Störung des

38
00:01:35,820 --> 00:01:37,773
öffentlichen Friedens einzusetzen.

39
00:01:37,940 --> 00:01:40,626
Und was es für unsere Demokratie
bedeutet, wenn sich

40
00:01:40,626 --> 00:01:43,830
der Chilling-Effekt wie ei
 großer Schatten über unsere

41
00:01:43,830 --> 00:01:46,926
Gesellschaft legt: Durch
ausgehebelte Verschlüsselung,

42
00:01:46,920 --> 00:01:50,040
den Verlust vertrauenswürdiger Kommunikation und

43
00:01:50,040 --> 00:01:52,406
die Abwesenheit von Privatsphäre.

44
00:01:52,400 --> 00:01:55,446
Aber es geht um den Schutz unserer Kinder.

45
00:01:55,446 --> 00:01:57,613
Und die Frage ist, ob es nicht auch andere

46
00:01:57,610 --> 00:02:00,560
Möglichkeiten gibt, Kinder besser
vor Missbrauch zu schützen.

47
00:02:00,560 --> 00:02:02,280
Also lassen wir die zu Wort kommen,

48
00:02:02,280 --> 00:02:05,080
die bei diesem Thema Expertise und Erfahrung haben:

49
00:02:05,080 --> 00:02:07,553
Kinderschutz-Verbände weisen darauf hin,

50
00:02:07,550 --> 00:02:09,300
dass die verschlüsselte Kommunikation

51
00:02:09,300 --> 00:02:10,800
in den meist verbreiteten Apps

52
00:02:10,800 --> 00:02:12,870
in diesem Bereich kaum eine Rolle spielt,

53
00:02:12,990 --> 00:02:15,210
da die Täter meist andere Kanäle nutzen,

54
00:02:15,210 --> 00:02:16,560
um Material zu tauschen.

55
00:02:17,130 --> 00:02:18,993
In Deutschland haben wir zusätzlich den Fall,

56
00:02:18,990 --> 00:02:21,520
dass rund die Hälfte der Verfahren gegen Kinder

57
00:02:21,520 --> 00:02:24,760
und Jugendliche selbst laufen,
obwohl sie einvernehmlich

58
00:02:24,760 --> 00:02:27,820
und legal eigene intime Bilder getauscht haben.

59
00:02:27,900 --> 00:02:29,393
Ein Problem, das sich übrigens durch

60
00:02:29,430 --> 00:02:31,740
die Chatkontrolle noch rapide verstärken könnte,

61
00:02:31,800 --> 00:02:34,520
da das geplante System
sich auf die Kanäle fokussiert,

62
00:02:34,520 --> 00:02:35,766
die vor allem von Kindern und

63
00:02:35,760 --> 00:02:37,506
Jugendlichen häufig genutzt werden.

64
00:02:37,920 --> 00:02:40,946
Kinderschutzverbände fordern:
eine bessere Ausstattung und

65
00:02:40,946 --> 00:02:42,990
mehr Fachkräfte für die Ämter, die für

66
00:02:42,990 --> 00:02:44,853
den Schutz der Kinder zuständig sind.

67
00:02:44,870 --> 00:02:48,320
außerdem fehlt eine verplichtende
Ausbildung im Kinderschutz.

68
00:02:48,360 --> 00:02:50,910
Kinder müssten wissen, wo es Hilfe gibt, was

69
00:02:50,910 --> 00:02:53,610
Erwachsene dürfen und was nicht. Und um

70
00:02:53,610 --> 00:02:56,460
mögliche Betroffene schneller
erkennen zu können, muss

71
00:02:56,460 --> 00:03:00,160
Lehr- und Erziehungspersonal geschult
und sensibilisiert werden.

72
00:03:00,160 --> 00:03:03,666
Vor diesem Hintergrund fordern wir,
dass das Geld, das durch

73
00:03:03,660 --> 00:03:07,046
dieses Vorhaben in eine dystopische
Überwachungs-Maschinerie

74
00:03:07,046 --> 00:03:09,540
gelenkt werden soll, stattdessen in echten

75
00:03:09,540 --> 00:03:12,426
und effektiven Kinderschutz investiert wird.

76
00:03:12,480 --> 00:03:14,766
Das gibt uns als Gesellschaft die Möglichkeit,

77
00:03:14,766 --> 00:03:17,760
zwei Ziele zu erreichen: Kinder und Jugendliche

78
00:03:17,820 --> 00:03:20,640
werden effektiver und nachhaltige
 vor Missbrauch geschützt.

79
00:03:20,820 --> 00:03:23,480
Die vertrauliche Kommunikation und die Privatsphäre,

80
00:03:23,480 --> 00:03:26,726
die durch die Grundrechte-Charta
garantiert ist, bleibt erhalten.

81
00:03:26,913 --> 00:03:30,993
Hilf uns, das Gesetz zu verhindern,
indem du dieses Video verbreitest

82
00:03:31,060 --> 00:03:33,780
und geh auf chat-kontrolle.eu.,

83
00:03:33,840 --> 00:03:37,920
um weiter informiert zu bleiben
und zu erfahren, was du tun kannst.

